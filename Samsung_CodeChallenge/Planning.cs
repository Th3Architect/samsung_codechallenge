/*
needs:
pass a url to the solution that will include the refrence and the dataset

find an exact match of the reference in the dataset, allowing for the pattern to be rotated

do not include subsets of a pattern


example json:
{
	"reference" : "00000000 00000000 00111100 00000100 00000100 00000000 00000000 00000000",
	"pointclouds" : [
		"00000000 00000000 00000000 00000000 00000000 11110000 00010000 00010000",
		"00000000 00000010 00000010 00000010 00001110 00000000 00000000 00000000",
		"10000000 10000000 11110000 00000000 00000000 00000000 00000000 00000000",
		"00000000 00000000 00111110 00000010 00000010 00000000 00000000 00000000",
		"00000000 00000000 00000100 00000100 00111100 00000000 00000000 00000000",
	]
}

*********************************************************************

Planning:

*********************************************************************

string[] rows = str.split(' '); 
this will split the string into an array of each rows.[0]

for i < rows.length
if string does not contain 1 
if a 1 hasn't been found yet delete
if a 1 has been found, store the index
if we reach the end of the array and no more 1s are found, delete everything in the array after that index
end for 

this would leave you with the following from the example above:
00111100 00000100 0000010

rows.[0]

String.IndexOf(1) will return the first occurance
String.LastIndexOf(1) will return the last occurance

var firstIndex
var lastIndex

for i < rows.length
if string.IndexOf < firstIndex
firstIndex = String.IndexOf()
if string.lastIndexOf < firstIndex
lastIndex = String.lastIndexOf()
end for

for i < rows.length
string.Substring(firstIndex, lastIndex - firstIndex + 1)
end for

this would leave you with the following:
1111 0001 0001


now that the pattern has been extracted from the reference we can use it to check our pointclouds
we can also rotate our smaller pattern array faster now as well
to rotate the pattern array it will be easier to store in a 2d array

int[,] Pattern2dArray = new int[rows.Lenght, rows[0].length]

before I rotate I want to check my current orientation
I think it makes sense to also reduce the pointcloud data to only include points we can compare
Simply turn the previous steps into a method we can feed a string into, knowing that our strings will always be an 8x8 grid of data
I think it makes sense for this method to return the data as a Pattern2dArray

this would leave you with the following for the first point cloud
1111 0001 0001

then simply check if arrays are identical
because of the way I am reducing the array down to only the data I need it will stop subsets from being found unless I compare in a different fashion

pattern2dArray.Equals(pointcloud2DArray)

this would return true, so i want to store my results, otherwise I would rotate and try compare again

need to research how to rotate matrix

keep track of rotation, if nothing matches after 3 rotations then set result to false

after iterating through the pointclouds array, write results to console
*/
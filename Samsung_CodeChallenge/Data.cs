﻿using System;

namespace Samsung_CodeChallenge
{
    [Serializable]
    public struct Data
    {
        public string reference;
        public string[] pointclouds;
    }
}

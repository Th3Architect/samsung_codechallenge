﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Newtonsoft.Json;
using Exception = System.Exception;

namespace Samsung_CodeChallenge
{
    class Program
    {
        static void Main(string[] args)
        {
            do
            {
                while (!Console.KeyAvailable)
                {
                    string inputString;
                    Console.WriteLine("\nPlease enter a URL.");
                    inputString = Console.ReadLine();

                    string json = "";
                    if (Uri.IsWellFormedUriString(inputString, UriKind.Absolute))
                    {
                        json = new WebClient().DownloadString(inputString);
                    }
                    else if (json.Length == 0)
                    {
                        Console.WriteLine("Input is invalid.");
                        break;
                    }

                    var data = JsonConvert.DeserializeObject<Data>(json);

                    string[] reference = data.reference.Split(' ');
                    var pattern2DArray = ParsePattern(reference);

                    var pointcloudCount = data.pointclouds.Length;
                    var results = new List<int>();
                    for (int i = 0; i < pointcloudCount; i++)
                    {
                        string[] pointcloud = data.pointclouds[i].Split(' ');
                        var pointcloud2dArray = ParsePattern(pointcloud);

                        var result = ComparePattern(pattern2DArray, pointcloud2dArray);

                        if (result == false)
                        {
                            //attempt to rotate a maximum of 3 times and check each time
                            int[,] rotatedPattern = pattern2DArray;
                            for (int j = 0; j < 3; j++)
                            {
                                rotatedPattern = RotatePattern(rotatedPattern);
                                result = ComparePattern(rotatedPattern, pointcloud2dArray);

                                if (result == true)
                                {
                                    results.Add(i);
                                }
                            }
                        }
                        else
                        {
                            results.Add(i);
                        }
                    }

                    //print results to console
                    foreach (var result in results)
                    {
                        Console.WriteLine(result);
                    }
                }
            } while (Console.ReadKey(true).Key != ConsoleKey.Escape);
        }

        private static int[,] ParsePattern(string[] patternRows)
        {
            //parse the pattern from its surrounding data
            bool patternFound = false;
            int patternEndIndex = 0;
            for (int i = 0; i < patternRows.Length; i++)
            {
                if (!patternRows[i].Contains("1"))
                {
                    if (!patternFound)
                    {
                        patternRows = patternRows.Skip(1).ToArray();
                        i--;
                    }
                }
                else
                {
                    if (patternEndIndex < i)
                    {
                        patternEndIndex = i;
                    }
                    patternFound = true;
                }
            }
            //remove any empty data at end of array
            Array.Resize(ref patternRows, patternEndIndex + 1);

            //truncute string to create smallest data array
            int firstIndex = 7;
            int lastIndex = 0;
            for (int i = 0; i < patternRows.Length; i++)
            {
                if (patternRows[i].Contains("1"))
                {
                    if (patternRows[i].IndexOf("1") < firstIndex)
                    {
                        firstIndex = patternRows[i].IndexOf("1");
                    }

                    if (patternRows[i].LastIndexOf("1") > lastIndex)
                    {
                        lastIndex = patternRows[i].LastIndexOf("1");
                    }
                }
            }

            for (int i = 0; i < patternRows.Length; i++)
            {
                var s = patternRows[i];
                patternRows[i] = s.Substring(firstIndex, (lastIndex - firstIndex + 1));
            }

            //convert into a matrix and return
            int[,] patternMatrix = new int[patternRows.Length, patternRows[0].Length];
            for (int i = 0; i < patternRows.Length; i++)
            {
                for (int j = 0; j < patternRows[i].Length; j++)
                {
                    patternMatrix[i, j] = Int32.Parse(patternRows[i].Substring(j, 1));
                }
            }

            return patternMatrix;
        }

        private static bool ComparePattern(int[,] pattern, int[,] pointcloud)
        {
            bool result = true;

            for (int i = 0; i < pattern.GetLength(0); i++)
            {
                for (int j = 0; j < pattern.GetLength(1); j++)
                {
                    try
                    {
                        if (pattern[i, j] != pointcloud[i, j])
                        {
                            result = false;
                        }
                    }
                    catch (Exception e)
                    {
                        result = false;
                    };
                }
            }
            return result;
        }

        private static int[,] RotatePattern(int[,] pattern)
        {
            int width;
            int height;
            int[,] newArray;

            width = pattern.GetUpperBound(0) + 1;
            height = pattern.GetUpperBound(1) + 1;
            newArray = new int[height, width];

            for (int row = 0; row < height; row++)
            {
                for (int col = 0; col < width; col++)
                {
                    int newRow;
                    int newCol;

                    newRow = col;
                    newCol = height - (row + 1);

                    newArray[newCol, newRow] = pattern[col, row];
                }
            }

            return newArray;
        }
    }
}
